package qaops.automation.api.teste;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Test;
import qaops.automation.api.dominio.Usuario;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TesteUsarioOutraOpcao extends TesteBase {

    private static final String BASE_URI = "https://reqres.in";
    private static final String BASE_PATH = "/api";

    private static final String LISTA_USUARIOS_ENDPOINT = "/users";
    private static final String CRIA_USUARIO_ENDPOINT = "/user";
    private static final String MOSTRAR_USUARIO_ENDPOINT = "/users/{userId}";

    @BeforeClass
    public static void setup(){
        // Função RestAssured para gerar logo sempre que um teste falhar
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Test
    public void testeListarMetaDadosDoUsuario() {
        String uri = getUri(LISTA_USUARIOS_ENDPOINT);
        given().
                // Setando os parâmetros da URI dinamicamente por parâmetros
                params("page", "2").
        when().
                get(uri).
        then().
                contentType(ContentType.JSON).
                statusCode(HttpStatus.SC_OK).
                body("page", is(2)).
                body("data", is(notNullValue()));
    }

    @Test
    public void testeCriarUsuarioComSucesso(){
        String uri = getUri(CRIA_USUARIO_ENDPOINT);

        Map<String, String>usuario = new HashMap<>();
        usuario.put("name", "Cassio");
        usuario.put("job", "Qa");

        given().
                contentType(ContentType.JSON).
                body(usuario).
        when().
                post(uri).
        then().
                contentType(ContentType.JSON).
                statusCode(HttpStatus.SC_CREATED).
                body("name", is("Cassio"));
    }

    @Test
    public void testeDaListaDeRetornoIgualAoPerPage() {
        String uri = getUri(LISTA_USUARIOS_ENDPOINT);

        int paginaEsperada = 2;
        int perPageEsperado = retornaPerPageEsperado(paginaEsperada);
        given().
                // Setando os parâmetros da URI dinamicamente por parâmetros
                params("page", paginaEsperada).
        when().
                get(uri).
        then().
                contentType(ContentType.JSON).
                statusCode(HttpStatus.SC_OK).
        body(
                "page", is(paginaEsperada),
                "data.size()", is(perPageEsperado), //Método size() dentro do collection Data
                "data.findAll{ it.avatar.startsWith('https://reqres.in/img/faces')}.size()", is(perPageEsperado) // Acha tudo, dada uma condição e retorna num array
        );
    }

    @Test
    public void testeMostraUsuarioEspecifico(){
        String uri = getUri(MOSTRAR_USUARIO_ENDPOINT);
        Usuario usuario = given().
                pathParam("userId", 2).
        when().
                get(uri).
        then().
                contentType(ContentType.JSON).
                statusCode(HttpStatus.SC_OK).
        extract().
            body().jsonPath().getObject("data", Usuario.class);

        assertThat(usuario.getEmail(), containsString("@reqres.in"));
        assertThat(usuario.getName(), is("Janet"));
        assertThat(usuario.getLastName(), is("Weaver"));
    }

    // Método para pegar a quantidade de retorno por página

    private int retornaPerPageEsperado(int page) {
        String uri = getUri(LISTA_USUARIOS_ENDPOINT);
        int perPageEsperado = given().
                params("page", page).
            when().
                get(uri).
            then().
                statusCode(HttpStatus.SC_OK).
                extract()
                    .path("per_page");
        return perPageEsperado;
    }

    private String getUri(String endpoit) {
        String uri = BASE_URI + BASE_PATH + endpoit;
        return uri;
    }
}