package qaops.automation.api.teste;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;

public class TesteBase {
    @BeforeClass
    public static void setup(){
        // Função RestAssured para gerar logo sempre que um teste falhar
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        // Configurando URI e Path diretamente em métodos RestAssured
        baseURI = "https://reqres.in";
        basePath = "/api";

        // Criando um RequestSpecification para o setup, setando um "ContentType para JSON"
        RestAssured.requestSpecification = new RequestSpecBuilder().
                setContentType(ContentType.JSON).
                build();

        RestAssured.responseSpecification = new ResponseSpecBuilder().
                expectContentType(ContentType.JSON).
                build();
    }
}
