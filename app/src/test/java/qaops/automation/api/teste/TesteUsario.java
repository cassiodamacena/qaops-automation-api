package qaops.automation.api.teste;

import org.apache.http.HttpStatus;
import org.junit.Test;
import qaops.automation.api.dominio.Usuario;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

public class TesteUsario extends TesteBase {

    private static final String LISTA_USUARIOS_ENDPOINT = "/users";
    private static final String CRIA_USUARIO_ENDPOINT = "/user";
    private static final String MOSTRAR_USUARIO_ENDPOINT = "users/{userId}";

    @Test
    public void testeListarMetaDadosDoUsuario() {
        given().
                // Setando os parâmetros da URI dinamicamente por parâmetros
                params("page", "2").
        when().
                get(LISTA_USUARIOS_ENDPOINT).
        then().
                statusCode(HttpStatus.SC_OK).
                body("page", is(2)).
                body("data", is(notNullValue()));
    }

    @Test
    public void testeCriarUsuarioComSucesso(){

        Map<String, String>usuario = new HashMap<>();
        usuario.put("name", "Cassio");
        usuario.put("job", "Qa");

        given().
                body(usuario).
        when().
                post(CRIA_USUARIO_ENDPOINT).
        then().
                statusCode(HttpStatus.SC_CREATED).
                body("name", is("Cassio"));
    }

    @Test
    public void testeDaListaDeRetornoIgualAoPerPage() {
        int paginaEsperada = 2;
        int perPageEsperado = retornaPerPageEsperado(paginaEsperada);
        given().
                // Setando os parâmetros da URI dinamicamente por parâmetros
                params("page", paginaEsperada).
        when().
            get(LISTA_USUARIOS_ENDPOINT).
        then().
            statusCode(HttpStatus.SC_OK).
        body(
                "page", is(paginaEsperada),
                "data.size()", is(perPageEsperado), //Método size() dentro do collection Data
                "data.findAll{ it.avatar.startsWith('https://reqres.in/img/faces')}.size()", is(perPageEsperado) // Acha tudo, dada uma condição e retorna num array
        );
    }

    @Test
    public void testeMostraUsuarioEspecifico(){
        Usuario usuario = given().
                pathParam("userId", 2).
        when().
                get(MOSTRAR_USUARIO_ENDPOINT).
        then().
                statusCode(HttpStatus.SC_OK).
        extract().
            body().jsonPath().getObject("data", Usuario.class);

        assertThat(usuario.getEmail(), containsString("@reqres.in"));
        assertThat(usuario.getName(), is("Janet"));
        assertThat(usuario.getLastName(), is("Weaver"));
    }

    // Método para pegar a quantidade de retorno por página
    private int retornaPerPageEsperado(int page) {
        int perPageEsperado = given().
                params("page", page).
            when().
                get(LISTA_USUARIOS_ENDPOINT).
            then().
                statusCode(HttpStatus.SC_OK).
                extract()
                    .path("per_page");
        return perPageEsperado;
    }
}