package qaops.automation.api.teste;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Test;
import qaops.automation.api.dominio.Usuario;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;


public class TesteRegistro extends TesteBase {

    public static final String REGISTRA_USUARIO_ENDPOINT = "/register";

    @BeforeClass
    public static void setupRegistro(){
        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(HttpStatus.SC_BAD_REQUEST)
                .build();
    }

    @Test
    // Teste deveria está no TesteLogin, porém está aqui só para mostrar a fucnionalidade de multiplos setups e ResponseSpec'
    public void testeNaoEfetuaRegistroSemInformarSenha(){
        Usuario usuario = new Usuario();
        usuario.setEmail("cassio@email.com");

        given().
                body(usuario).
        when().
                post(REGISTRA_USUARIO_ENDPOINT).
        then().
                body("error", is("Missing password"));
    }

    @Test
    public void testeLoginNaoRealizadoSemInformarSenha(){
        Usuario usuario = new Usuario();
        usuario.setEmail("cassio@email.com");

        given().
                body(usuario).
                when().
                post(REGISTRA_USUARIO_ENDPOINT).
                then().
                body("error", is("Missing password"));
    }

}
