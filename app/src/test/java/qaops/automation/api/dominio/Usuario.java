package qaops.automation.api.dominio;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data               // Adiciona os Getters e Setters automaticamente, além de outras funcionalidades
@AllArgsConstructor // Cria um contrutor populado automaticamente
@NoArgsConstructor  // Cria um construor padrão automaticamente
public class Usuario {

    @JsonAlias("first_name")
    private String name;
    private String job;
    private String email;
    @JsonAlias("last_name")
    private String lastName;
}
